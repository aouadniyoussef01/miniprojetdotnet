﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrontEndMicroService.BackAdmin.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string lienImage { get; set; }
        [NotMapped]
        public byte[] FileDetail { get; set; }

    }
}
