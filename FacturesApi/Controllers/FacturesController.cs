using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FacturesApi.Data;
using FacturesApi.Models;
using Newtonsoft.Json;
using MassTransit.Transports;
using Microsoft.Extensions.Hosting.Internal;
using static System.Reflection.Metadata.BlobBuilder;
using System.Data;
using FacturesApi.Extension;
using Microsoft.Reporting.NETCore;

namespace FacturesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacturesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public FacturesController(ApplicationDbContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: api/Factures
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Facture>>> GetFacture()
        {
            var factures = await _context.Facture.Include(a => a.LigneFactures).ThenInclude(a => a.Piece).Include(a => a.Intervention).ThenInclude(r => r.Reclamation).ToListAsync();
            var jsonString = JsonConvert.SerializeObject(factures, Formatting.None, new JsonSerializerSettings()
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
            List<Facture> ordd = JsonConvert.DeserializeObject<List<Facture>>(jsonString);

            return ordd;
        }

        // GET: api/Factures/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Facture>> GetFacture(int id)
        {
            var facture = await _context.Facture.Include(a=>a.LigneFactures).ThenInclude(a=>a.Piece).Include(a=>a.Intervention).ThenInclude(r=>r.Reclamation).FirstOrDefaultAsync(a=>a.Id==id);

            if (facture == null)
            {
                return NotFound();
            }
            var jsonString = JsonConvert.SerializeObject(facture, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            Facture ordd = JsonConvert.DeserializeObject<Facture>(jsonString);

            return ordd;
        }

        // PUT: api/Factures/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFacture(int id, Facture facture)
        {
            if (id != facture.Id)
            {
                return BadRequest();
            }

            _context.Entry(facture).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FactureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Factures
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Facture>> PostFacture(Facture facture)
        {
            _context.Facture.Add(facture);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetFacture", new { id = facture.Id }, facture);
            return facture;
        }

        // DELETE: api/Factures/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFacture(int id)
        {
            var facture = await _context.Facture.Include(a=>a.LigneFactures).FirstOrDefaultAsync(i=>i.Id==id);
            if (facture == null)
            {
                return NotFound();
            }

            _context.Facture.Remove(facture);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FactureExists(int id)
        {
            return _context.Facture.Any(e => e.Id == id);
        }
        //GET: api/Factures/5
        [HttpGet("ReportingFacture/{idFacture}")]
        public async Task<byte[]> ReportingFacture(int idFacture)
        {
            var lr = new LocalReport
            {
                ReportPath = _webHostEnvironment.ContentRootPath+ "/Reporting/Order.rdlc",
                EnableExternalImages = true,
                EnableHyperlinks = true
            };
            var facture = await _context.Facture.Where(a => a.Id == idFacture).Include(a => a.Intervention).ThenInclude(a => a.Reclamation).FirstOrDefaultAsync();
            var lgFacture = await _context.LigneFacture.Where(a => a.FactureId == idFacture).Include(a => a.Piece).ToListAsync();
            var i = 1;
            var NPSData = (from Ls in lgFacture
                           select new
                           {
                               N = i++,
                               Img = "",
                               Title = Ls.Piece.Name,
                               Qte = Ls.Qte,
                               PriceU = Ls.Prix.ToString("n3"),
                               PrixTotal = (Ls.Prix * Ls.Qte).ToString("n3")
                           }).ToList();

            Object[] ligneordeer = NPSData.ToArray();

            DataTable ordersDatatable = DataTableTools.ConvertToDataTable(ligneordeer);
            var data = ordersDatatable;
            lr.DataSources.Add(new ReportDataSource("DataSet", ordersDatatable));

            ReportParameter EmailUser = new ReportParameter("EmailUser", facture.Intervention.Reclamation.Email);
            lr.SetParameters(EmailUser);
            ReportParameter NomUser = new ReportParameter("NomUser", facture.Intervention.Reclamation.Name);
            lr.SetParameters(NomUser);


            ReportParameter TotalPrix = new ReportParameter("TotalPrix", (facture.Intervention.garantie?0: facture.LigneFactures.Sum(a => a.Prix * a.Qte)).ToString("n3"));
            lr.SetParameters(TotalPrix);
            lr.Refresh();

            Warning[] warnings;
            string[] streamIds;
            string contentType;
            string encoding;
            string extension;
            byte[] bytes = lr.Render("PDF", null, out contentType, out encoding, out extension, out streamIds, out warnings);
            //File.WriteAllBytes("D:/TestOrderERY.pdf", bytes);
            return bytes;
        }
        private string MapPath(string path)
        {
            return Path.Combine(
                (string)AppDomain.CurrentDomain.GetData("/"), path);
        }
    }
}
