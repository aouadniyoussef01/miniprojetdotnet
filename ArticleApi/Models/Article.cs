﻿using MassTransit.MessageData.Values;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ArticleApi.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string lienImage { get; set; } 
    }
}
