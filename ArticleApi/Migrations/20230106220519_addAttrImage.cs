﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ArticleApi.Migrations
{
    public partial class addAttrImage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "lienImage",
                table: "Article",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "lienImage",
                table: "Article");
        }
    }
}
